﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTimeSheet.ViewModel;
using timesheet.business;
using timesheet.model;

namespace TaskTimeSheet.Controllers
{
    [Route("api/timesheet")]
    [ApiController]
    public class EmployeeTimeSheetController : ControllerBase
    {
        private readonly EmployeeTimeSheetService timesheetService;
        // Create a field to store the mapper object
        private readonly IMapper _mapper;
        public EmployeeTimeSheetController(EmployeeTimeSheetService TimeSheetService, IMapper mapper)
        {
            this.timesheetService = TimeSheetService;
            _mapper = mapper;
        }
        [HttpGet("getemployeetimesheetbyid")]
        public IActionResult GetEmployeeTimeSheet(int EmployeeId)
        {
            var TimeSheet = this.timesheetService.EmployeeTimeSheet(EmployeeId).ToList();
            return new ObjectResult(_mapper.Map<List<EmployeeTimeSheet>, List<EmployeeTimeSheetViewModel>>(TimeSheet));
        }
        [HttpPost("addemployeetimesheet")]
        public IActionResult AddEmployeeTimeSheet(AddEmployeeTimeSheetViewModel NewTimeSheetModel)
        {
            var IsAdd = this.timesheetService.AddEmployeeTimeSheet(_mapper.Map<AddEmployeeTimeSheetViewModel, EmployeeTimeSheet>(NewTimeSheetModel));
            return new ObjectResult(IsAdd);
        }
    }
}
