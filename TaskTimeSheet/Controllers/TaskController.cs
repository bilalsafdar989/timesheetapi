﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTimeSheet.ViewModel;
using timesheet.business;

namespace TaskTimeSheet.Controllers
{
    [Route("api/task")]
    [ApiController]
    public class TaskController : ControllerBase
    {
        private readonly TaskService taskService;
        // Create a field to store the mapper object
        private readonly IMapper _mapper;
        public TaskController(TaskService TaskService, IMapper mapper)
        {
            this.taskService = TaskService;
            _mapper = mapper;
        }
        [HttpGet("getall")]
        public IActionResult GetAll(string text)
        {
            var Tasks = this.taskService.GetTasks().ToList();
            return new ObjectResult(_mapper.Map<List<timesheet.model.Task>, List<TaskViewModel>>(Tasks));
        }
    }
}
