﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaskTimeSheet.ViewModel;
using timesheet.business;
using timesheet.model;

namespace TaskTimeSheet.Controllers
{
    [Route("api/employee")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        private readonly EmployeeService employeeService;
        // Create a field to store the mapper object
        private readonly IMapper _mapper;
        public EmployeeController(EmployeeService employeeService, IMapper mapper)
        {
            this.employeeService = employeeService;
            _mapper = mapper;
        }
        [HttpGet("getall")]
        public IActionResult GetAll()
        {
            var Employees = this.employeeService.GetEmployees().ToList();
            return new ObjectResult(_mapper.Map<List<Employee>, List<EmployeeViewModel>>(Employees));
            
        }
    }
}
