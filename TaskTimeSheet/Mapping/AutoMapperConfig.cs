﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using timesheet.model;
using TaskTimeSheet.ViewModel;
using AutoMapper;
namespace TaskTimeSheet.Mapping
{
    public class AutoMapperConfig : Profile
    {
        public AutoMapperConfig()
        {
            CreateMap<Employee, EmployeeViewModel>()
            .ForMember(
            dest => dest.Name,
            opts => opts.MapFrom(src => src.Name))
           .ForMember(
            dest => dest.Id,
            opts => opts.MapFrom(src => src.Id))
            .ForMember(
            dest => dest.Code,
            opts => opts.MapFrom(src => src.Code))
            .ForMember(
            dest => dest.WeeklyEffortTotal,
            opts => opts.MapFrom(src => src.EmployeeTimeSheet.Count()>0 ? src.EmployeeTimeSheet.SelectMany(x=> x.DayEffortLogs).Sum(z=> z.Value): 0))
            .ForMember(
            dest => dest.WeeklyEffortTotalAvg,
            opts => opts.MapFrom(src => src.EmployeeTimeSheet.Count() > 0 ? src.EmployeeTimeSheet.SelectMany(x => x.DayEffortLogs).Average(z => z.Value) : 0));

            CreateMap<timesheet.model.Task, TaskViewModel>()
                  .ForMember(
            dest => dest.Name,
            opts => opts.MapFrom(src => src.Name))
           .ForMember(
            dest => dest.Id,
            opts => opts.MapFrom(src => src.Id))
            .ForMember(
            dest => dest.Description,
            opts => opts.MapFrom(src => src.Description));

            CreateMap<EmployeeTimeSheet, EmployeeTimeSheetViewModel>()
                 .ForMember(
           dest => dest.EmployeeId,
           opts => opts.MapFrom(src => src.EmployeeId))
          .ForMember(
           dest => dest.Id,
           opts => opts.MapFrom(src => src.Id))
           .ForMember(
           dest => dest.TaskId,
           opts => opts.MapFrom(src => src.TaskId))
             .ForMember(
           dest => dest.DayEffortLogs,
           opts => opts.MapFrom(src => src.DayEffortLogs));

            CreateMap<EmployeeTimeSheetViewModel, EmployeeTimeSheet>()
                  .ForMember(
           dest => dest.EmployeeId,
           opts => opts.MapFrom(src => src.EmployeeId))
          .ForMember(
           dest => dest.Id,
           opts => opts.MapFrom(src => src.Id))
           .ForMember(
           dest => dest.TaskId,
           opts => opts.MapFrom(src => src.TaskId))
             .ForMember(
           dest => dest.DayEffortLogs,
           opts => opts.MapFrom(src => src.DayEffortLogs));

            CreateMap<DayEffortLog, DayEffortLogViewModel>()
                  .ForMember(
            dest => dest.EmployeeTimeSheetId,
            opts => opts.MapFrom(src => src.EmployeeTimeSheetId))
           .ForMember(
            dest => dest.Id,
            opts => opts.MapFrom(src => src.Id))
            .ForMember(
            dest => dest.Value,
            opts => opts.MapFrom(src => src.Value))
             .ForMember(
            dest => dest.DayName,
            opts => opts.MapFrom(src => src.DayName));

            CreateMap<DayEffortLog, AddDayEffortLogViewModel>()
                  .ForMember(
            dest => dest.EmployeeTimeSheetId,
            opts => opts.MapFrom(src => src.EmployeeTimeSheetId))
           .ForMember(
            dest => dest.Id,
            opts => opts.MapFrom(src => src.Id))
            .ForMember(
            dest => dest.Value,
            opts => opts.MapFrom(src => src.Value))
             .ForMember(
            dest => dest.DayName,
            opts => opts.MapFrom(src => src.DayName));

            CreateMap<EmployeeTimeSheet, AddEmployeeTimeSheetViewModel>()
               .ForMember(
         dest => dest.EmployeeId,
         opts => opts.MapFrom(src => src.EmployeeId))
        .ForMember(
         dest => dest.Id,
         opts => opts.MapFrom(src => src.Id))
         .ForMember(
         dest => dest.TaskId,
         opts => opts.MapFrom(src => src.TaskId))
           .ForMember(
         dest => dest.DayEffortLogs,
         opts => opts.MapFrom(src => src.DayEffortLogs));

            CreateMap<AddEmployeeTimeSheetViewModel, EmployeeTimeSheet>()
                  .ForMember(
           dest => dest.EmployeeId,
           opts => opts.MapFrom(src => src.EmployeeId))
          .ForMember(
           dest => dest.Id,
           opts => opts.MapFrom(src => src.Id))
           .ForMember(
           dest => dest.TaskId,
           opts => opts.MapFrom(src => src.TaskId))
             .ForMember(
           dest => dest.DayEffortLogs,
           opts => opts.MapFrom(src => src.DayEffortLogs));
        }
    }
}
