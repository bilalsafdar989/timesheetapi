﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskTimeSheet.ViewModel
{
    public class EmployeeViewModel
    {
        
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int WeeklyEffortTotal { get; set; }
        public decimal WeeklyEffortTotalAvg { get; set; }
    }
}
