﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskTimeSheet.ViewModel
{
    public class EmployeeTimeSheetViewModel
    {
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public virtual EmployeeViewModel Employee { get; set; }
        public int TaskId { get; set; }
        public virtual TaskViewModel Task { get; set; }
        public virtual ICollection<DayEffortLogViewModel> DayEffortLogs { get; set; }
    }
}
