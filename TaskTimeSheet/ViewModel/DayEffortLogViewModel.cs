﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaskTimeSheet.ViewModel
{
    public class DayEffortLogViewModel
    {
        public int Id { get; set; }
        public string DayName { get; set; }
        public int Value { get; set; }
        public int EmployeeTimeSheetId { get; set; }
    }
}
