﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using timesheet.data;
using timesheet.model;
namespace timesheet.business
{
    public class EmployeeTimeSheetService
    {
        public TimesheetDb db { get; }
        public EmployeeTimeSheetService(TimesheetDb dbContext)
        {
            this.db = dbContext;
        }

        public IQueryable<EmployeeTimeSheet> EmployeeTimeSheet(int EmployeeId)
        {
            return this.db.EmployeeTimeSheet.
              Include(_ => _.DayEffortLogs).
              Include(_ => _.Employee).
              Include(_ => _.Task).
              Where(x => x.EmployeeId== EmployeeId);
        }
        public bool AddEmployeeTimeSheet(EmployeeTimeSheet TimeSheet)
        {
            this.db.EmployeeTimeSheet.Add(TimeSheet);
            this.db.SaveChanges();
            return true;
;        }
    }
}
