﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    public class DayEffortLog
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        [Required]
        public string DayName { get; set;}
        [Required]
        public int Value { get; set; }
        public int EmployeeTimeSheetId { get; set; }
        public virtual EmployeeTimeSheet EmployeeTimeSheet { get; set; }
    }
}
