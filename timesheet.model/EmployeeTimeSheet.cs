﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace timesheet.model
{
    public class EmployeeTimeSheet
    {
        public EmployeeTimeSheet()
        {
            DayEffortLogs = new HashSet<DayEffortLog>();
        }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public int EmployeeId { get; set; }
        public virtual Employee Employee { get; set; }
        public int TaskId { get; set; }
        public virtual Task Task { get; set; }
        public virtual ICollection<DayEffortLog> DayEffortLogs { get;set;}
    }
}
